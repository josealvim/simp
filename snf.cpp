#include <snf.hpp>

#include <optional>
#include <iostream>


namespace simp {

static void print(Matrix const& M) {
	for (size_t i = 0; i < M.height; i++) {
		for (size_t j = 0; j < M.width;  j++) {
			std::cout << M[{i,j}] << "\t";
		}
		std::cout << std::endl;
	}
}

static
auto partialSNF(Matrix M) {
	struct _ {
		Entry  e;
		Matrix M;
	};

	if (is_empty(M) or is_zero(M))
		throw "bruh";

	while (! evenly_divides(M, smallest_entry(M).value) ) {
		M = reduce_smallest_entry(M);
	}

	auto pivot = smallest_entry(M);

	M = remove_pivots_row_neighbors(M, pivot.pos);
	M = remove_pivots_col_neighbors(M, pivot.pos); // we could ignore this
	
	return _ {
		.e = pivot, 
		.M = delete_pivots_cross(M, pivot.pos)
	};
}

std::vector<Integer> SmithNormalForm(Matrix M) {
	std::vector<Integer> values;

	while ( ! (is_empty(M) or is_zero(M)) ) {
		auto [entry, _M] = partialSNF(M);
		M = _M;

		values.push_back(entry.value);
	}

	for (auto & b : values) {
		if (b < 0)
			b = -b;
	}

	return values;
}

Entry smallest_entry(Matrix const& M) {
	std::optional<Entry> smallest;
	for (size_t i = 0; i < M.height; i++)
	for (size_t j = 0; j < M.width;  j++) {
		auto cur_val = M[{i, j}];
		if ( cur_val == 0 )
			continue;

		if ( !smallest.has_value() ) {
			smallest = Entry {
				.pos = {.i = i, .j = j},
				.value = cur_val
			};
		} 

		auto min_val = smallest.value().value;

		auto abs_cur_val = cur_val > 0 ? cur_val : - cur_val;
		auto abs_min_val = min_val > 0 ? min_val : - min_val;

		if ( abs_min_val > abs_cur_val ) {
			smallest = Entry {
				.pos = {.i = i, .j = j},
				.value = cur_val
			};
		}
	}

	// will throw if matrix is empty, of course.
	return smallest.value();
}

bool evenly_divides (Matrix const& M, Integer I) {
	for (size_t i = 0; i < M.height; i++)
	for (size_t j = 0; j < M.width;  j++) {
		if ( M[{i, j}] % I != 0 )
			return false;
	}

	return true;
} 

bool is_empty (Matrix const& M) {
	return M.width == 0 || M.height == 0;
}

bool is_zero  (Matrix const& M) {
	for (size_t i = 0; i < M.height; i++)
	for (size_t j = 0; j < M.width;  j++) {
		if (M[{i,j}] != 0)
			return false;
	}

	return true;
}

Matrix remove_pivots_row_neighbors (Matrix M, Position pivot) {
	auto [i, _] = pivot;
	for (size_t j = 0; j < M.width; j++) {
		if (j == pivot.j) 
			continue;
		auto entry = M[{i, j}];
		auto ratio = entry / M[pivot];

		M = add_col_j0_times_q_to_j1(M, pivot.j, -ratio, j);
	}

	return M;
}

Matrix remove_pivots_col_neighbors (Matrix M, Position pivot) {
	auto [_, j] = pivot;
	for (size_t i = 0; i < M.height; i++) {
		if (i == pivot.i)
			continue;
		auto entry = M[{i, j}];
		auto ratio = entry / M[pivot];

		M = add_row_i0_times_q_to_i1(M, pivot.i, -ratio, i);
	}	

	return M;
}

Matrix swap_col_j0_and_j1 (Matrix M, size_t j0, size_t j1) {
	for (size_t i = 0; i < M.height; i++) {
		auto   tmp = M[{i, j0}];
		M[{i, j0}] = M[{i, j1}];
		M[{i, j1}] = tmp;
	}

	return M;
} 

Matrix swap_row_i0_and_i1 (Matrix M, size_t i0, size_t i1) {
	for (size_t j = 0; j < M.width; j++) {
		auto   tmp = M[{i0, j}];
		M[{i1, j}] = M[{i1, j}];
		M[{i0, j}] = tmp;
	}

	return M;
}

Matrix delete_pivots_cross (Matrix M, Position pos) {
	Matrix new_M(M.height - 1, M.width - 1);

	M = swap_col_j0_and_j1(M, 0, pos.j);
	M = swap_row_i0_and_i1(M, 0, pos.i);

	for (size_t i = 0; i < M.height - 1; i++) 
	for (size_t j = 0; j < M.width  - 1; j++) {
		new_M[{i,j}] = M[{i+1, j+1}];
	}

	return new_M;
}

Matrix add_col_j0_times_q_to_j1(Matrix M, size_t j0, Integer q, size_t j1) {
	if (j0 == j1) 
		throw "can't add column to itself";
	
	for (size_t i = 0; i < M.height; i++) {
		M[{i, j1}] += q * M[{i, j0}];
	}

	return M;
}

Matrix add_row_i0_times_q_to_i1(Matrix M, size_t i0, Integer q, size_t i1) {
	if (i0 == i1) 
		throw "can't add row to itself";

	for (size_t j = 0; j < M.width; j++) {
		M[{i1, j}] += q * M[{i0, j}];
	}

	return M;
}

Entry reduction_candidate(Matrix const& M, Entry pivot) {
	// best candidates are on the pivot's cross
	for (size_t i = 0; i < M.height; i++) {
		auto value = M[{i, pivot.pos.j}];
		if ( value % pivot.value != 0 )
			return {
				.pos = {i, pivot.pos.j},
				.value = value
			};
	}

	// best candidates are on the pivot's cross
	for (size_t j = 0; j < M.width; j++) {
		auto value = M[{pivot.pos.i, j}];
		if ( value % pivot.value != 0 )
			return {
				.pos = {pivot.pos.i, j},
				.value = value
			};
	}

	// fallback to searching the matrix
	for (size_t i = 0; i < M.height; i++)
	for (size_t j = 0; j < M.width;  j++) {
		auto value = M[{i,j}];
		if ( value % pivot.value )
			return {
				.pos = {i,j},
				.value = value
			};
		else 
			continue;
	}

	throw "unreachable?";
}

Matrix reduce_smallest_entry (Matrix M) {
	auto pivot_entry = smallest_entry(M);

	// we first find the first entry which the pivot doesn't divide	
	auto nmult_entry = reduction_candidate(M, pivot_entry);

	// if the entry is in the pivot's cross, we can reduce the pivot
	// by subtracting a multiple of that entry's line from the pivot's
	bool same_row = (nmult_entry.pos.i == pivot_entry.pos.i);
	bool same_col = (nmult_entry.pos.j == pivot_entry.pos.j);
	
	auto floordiv = (nmult_entry.value) / pivot_entry.value;

	// since we subtract 
	// `(nmult_entry.value / pivot_entry.value) * pivot_entry.value`
	// from that entry, the resulting value is the remainder, which 
	// is strictly smaller than `pivot_entry.value`.
	if ( same_row ) {
		return add_col_j0_times_q_to_j1(
			M,
			pivot_entry.pos.j,
			- floordiv,
			nmult_entry.pos.j
		);
	}

	// same for if it's in the same column
	if ( same_col ) {
		return add_row_i0_times_q_to_i1(
			M,
			pivot_entry.pos.i,
			- floordiv,
			nmult_entry.pos.i
		);
	}

	// if the non-divisible element _isn't_ on the pivot's cross, we 
	// have a mild problem, we shall seek to go back one of the base 
	// cases.

	// we find the position of the element for which nmult and the 
	// pivot lie on the same cross:
	auto shared_cross_center = Position {
		.i = nmult_entry.pos.i, 
		.j = pivot_entry.pos.j
	};

	// since shared_cross_center is on the pivot's cross, and we have
	// the care of prioritizing reduction candidates on its cross, we 
	// know that the pivot divides all the entries on its cross.
	auto ratio = M[shared_cross_center]/pivot_entry.value;

	// we can then subtract `ratio` times the pivot's row from the 
	// candidate's row, and add _that_ to the pivot's row.
	M = add_row_i0_times_q_to_i1(
		M, 
		pivot_entry.pos.i,
		-ratio,
		shared_cross_center.i
	);

	M = add_row_i0_times_q_to_i1(
		M, 
		shared_cross_center.i,
		1,
		pivot_entry.pos.i
	);

	// now, matrix is such that the old pivot doesn't divide one of 
	// its cross members. So this means we can recurse (hopefully?).
	// Well we don't need to recurse here, SmithNormalForm will call
	// us repeatedly.
	return M;
}

}