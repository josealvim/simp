#include <matrix.hpp>
#include <snf.hpp>

#include <vector>
#include <iostream>
#include <sstream>
#include <string>
#include <fstream>

simp::Matrix from_list (
	size_t H, 
	size_t W, 
	std::vector<simp::Integer> v
) {
	simp::Matrix result = simp::Matrix(H, W);
	for (size_t i = 0; i < H; i++)
	for (size_t j = 0; j < W; j++) {
		result[{i,j}] = v[i*W + j];	
	}
	return result;
}

simp::Matrix parse_matrix(char const *arg) {
	std::string   line;
	std::ifstream file(arg);
	
	size_t height = 0;
	size_t width  = 0;
	std::vector<simp::Integer> v;
	while (std::getline(file, line)) {
		std::istringstream ss(line);
		size_t _width  = 0;
		
		do {
			std::string num;
			ss >> num;
			v.push_back(std::atoll(num.c_str()));
			_width++;
		} while (ss);
		width = _width;
		height++;
	}

	file.close();
	return from_list (height, width, v);
}

int main (int argc, char **argv) {
	if (argc != 2)
		exit(-1);

	auto problem = parse_matrix(argv[1]);
//	for (size_t i = 0; i < height; i++) {
//		for (size_t j = 0; j < width;  j++) {
//			std::cout << problem[{i,j}] << "\t";
//		}
//		std::cout << std::endl;
//	}

	auto normalized = simp::SmithNormalForm(problem);

	for (auto betti : normalized)
		std::cout << betti << " ";
	std::cout << std::endl;

	return 0;
}