# Simplexial Homology Helper-Thing™

[NO WARRANTIES spiel etc. etc.] 
This is free code written by a rando on the internet, trust it as far as you can throw it.

## Dependencies
* `make`
* `g++` or your favorite C++17 compiler

## Building
* Download the repository, preferably via `git clone [repo url]`;
* In the repository, run:
	```
	$ make all
	```

## Using this
If you have a file, say, `example.txt` containing a matrix 
of whitespace (space, tabs, etc) separated numbers:
```
-1 -1  0 -1  0 -1  0  0  0  0  0  0  0  0 -1
+1  0 -1  0  0  0  0 -1  0  0  0  0 +1 -1  0
0   0  0  0  0 +1 +1 +1 -1 -1  0  0  0  0  0
0   0  0  0  0  0  0  0  0 +1 +1 -1 -1  0 +1
0  +1 +1  0 -1  0  0  0 +1  0  0  0  0  0  0
0   0  0 +1 +1  0 -1  0  0  0 -1 +1  0 +1  0
```
then you may call 
```
./main.out matrix.txt
```
and it will return you a list of integers corresponding 
to the values on the diagonal of that matrix's Smith Normal
Form. 

## Problems
Don't leave empty lines like this:
```
1 0 0 
2 3 4
						[empty line]
```
because extra lines will mess with the matrix parser
I wrote (very poorly). 

Don't put spaces between signs and numbers, the program gets
very angry and it ***will*** break things.

## Disclaimer
This software is probably buggy and is certainly not written
well and might not be right at all! Who knows? I don't. 
Neither do you. 

Found an issue? Open an Issue. Thank you!

## Licensing 
**GPL3**
