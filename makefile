DEBUG ?= 
CXX    = g++ -std=c++17 -I. ${DEBUG}

all: main.out
	@echo Done!

main.out: main.o snf.o
	${CXX}		-o $@ $^

.PHONY: clean

clean:
	find -type f -name "*.o"   -exec rm {} \;
	find -type f -name "*.out" -exec rm {} \;

%.o : %.cpp
	${CXX}	-c	-o $@ $^