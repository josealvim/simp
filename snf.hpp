#pragma once

#include <matrix.hpp>

#include <vector>

namespace simp {

struct Entry {
	Position pos;
	Integer value;
};

std::vector<Integer> SmithNormalForm(Matrix M);

Entry smallest_entry     (Matrix const&); // non empty matrix
Entry reduction_candidate(Matrix const&, Entry pivot);

bool evenly_divides (Matrix const&, Integer);
bool       is_empty (Matrix const&);
bool       is_zero  (Matrix const&);


Matrix add_col_j0_times_q_to_j1(Matrix, size_t j0, Integer q, size_t j1);
Matrix add_row_i0_times_q_to_i1(Matrix, size_t i0, Integer q, size_t i1);
Matrix swap_col_j0_and_j1      (Matrix, size_t j0, size_t j1);
Matrix swap_row_i0_and_i1      (Matrix, size_t i0, size_t i1);

Matrix remove_pivots_row_neighbors (Matrix, Position);
Matrix remove_pivots_col_neighbors (Matrix, Position);
Matrix delete_pivots_cross         (Matrix, Position);
Matrix       reduce_smallest_entry (Matrix);


}