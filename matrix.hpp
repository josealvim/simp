#pragma once

#include <vector> 
#include <cstddef> 

namespace simp {

using Integer = long long int;

struct Position {
	size_t i, j;
};

struct Matrix {
	Matrix(size_t H, size_t W) {
		data.resize(W*H);
		height = H;
		width  = W;
	}

	Integer const& operator[](Position p) const {
		if (p.i >= height || p.j >= width)
			throw "fuck me";
		return data[p.i * width + p.j];
	}

	Integer& operator[](Position p) {
		// scott meyers' trick
		return const_cast<Integer&>(
			std::as_const(*this)[p]
		);
	}

	size_t height, width;
	std::vector<Integer> data;
};

}